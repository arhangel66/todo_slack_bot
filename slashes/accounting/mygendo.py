from gendo import Gendo, __version__
import json


class MyGendo(Gendo):
    def respond(self, user, message, channel):
        if not message:
            return
        elif message == 'gendo version':
            self.speak("Gendo v{0}".format(__version__), channel)
            return
        for phrase, view_func, options in self.listeners:
            if phrase in message.lower():
                response = view_func(self.get_user_name(user), message, self.get_channel_name(channel), **options)
                if response:
                    if '{user.username}' in response:
                        response = response.replace('{user.username}',
                                                    self.get_user_name(user))
                    self.speak(response, channel)

    def get_channel_info(self, channel_id):
        channel = self.client.api_call('channels.info', channel=channel_id).decode('utf-8')
        return json.loads(channel)

    def get_channel_name(self, channel_id):
        channel = self.get_channel_info(channel_id)
        return channel.get('channel', {}).get('name')
