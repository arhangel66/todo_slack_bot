#!/usr/bin/env python
# -*- coding: utf-8 -*-
from myslackbot.settings import ACCEPTED_CHANNELS, ACCEPTED_USERS, BOT_API_KEY
from slashes.accounting.mygendo import MyGendo
from slashes.accounting.upworktool import get_today_sum

gendo = MyGendo(BOT_API_KEY)


@gendo.listen_for('day')
def day_report(user, message, channel):
    if (channel and channel in ACCEPTED_CHANNELS) or (user in ACCEPTED_USERS and channel is None):
        return get_today_sum()


@gendo.listen_for('test')
def testing(user, message, channel):
    if (channel and channel in ACCEPTED_CHANNELS) or (user in ACCEPTED_USERS and channel is None):
        return 'test completed'


if __name__ == '__main__':
    gendo.run()


# @gendo.cron('* * * * *')
# def some_task():
#     print 'HAY'
#     gendo.speak("Hay Ride!", "#accounting")
#     gendo.speak("Hay Ride2!", "#general")
