import datetime

import upwork
from django.conf import settings
from myslackbot.settings import U_KEY, U_SECRET, U_OAUTH_ACCESS_TOKEN, U_OAUTH_ACCESS_TOKEN_SECRET


def get_client():
    client = upwork.Client(U_KEY, U_SECRET,
                           oauth_access_token=U_OAUTH_ACCESS_TOKEN,
                           oauth_access_token_secret=U_OAUTH_ACCESS_TOKEN_SECRET)
    return client


# https://community.upwork.com/t5/API-Questions-Answers/Time-Report-Access-Denied/td-p/111189

def get_day_sum(day, client, company_id):
    query = "SELECT sum(charges) WHERE worked_on = '%s'" % day
    result = client.timereport.get_company_report(company_id, query)
    day_sum = result.get('table').get('rows')[0].get('c')[0].get('v')
    if day_sum is not None:
        day_sum = round(float(day_sum), 2)
    return day_sum


def get_day_sum_with_users(day, client, company_id):
    query = "SELECT provider_name, sum(charges) WHERE worked_on = '%s'" % day
    result = client.timereport.get_company_report(company_id, query)
    res = result.get('table', {}).get('rows')

    # fixed price payments
    query = "SELECT provider_name, amount, subtype WHERE date = '%s' and subtype = 'Fixed Price' and type='ARInvoice'" % day
    result = client.finreport.get_financial_entities_provider('570680', query)
    res += result.get('table', {}).get('rows')
    return res


def get_today_sum():
    client = get_client()
    today = (datetime.datetime.now() - datetime.timedelta(hours=12)).strftime("%Y-%m-%d")
    # yesterday = (datetime.datetime.now() + datetime.timedelta(days=-1)).strftime("%Y-%m-%d")

    company_id = u'_kaizen'
    today_res = get_day_sum_with_users(today, client, company_id)
    # yesterday_sum = get_day_sum(yesterday, client, company_id)
    # message = "%s = $%s, %s = $%s" % (today, today_sum, yesterday, yesterday_sum)
    message = format_to_text(today_res, today)
    return message


def get_client_from_ssh():
    client = upwork.Client(U_KEY, U_SECRET)
    #
    print "Please to this URL (authorize the app if necessary):"
    print client.auth.get_authorize_url()
    print "After that you should be redirected back to your app URL with " + \
          "additional ?oauth_verifier= parameter"

    verifier = raw_input('Enter oauth_verifier: ')
    #
    oauth_access_token, oauth_access_token_secret = \
        client.auth.get_access_token(verifier)
    print oauth_access_token, oauth_access_token_secret

    client = upwork.Client(U_KEY, U_SECRET,
                           oauth_access_token=oauth_access_token,
                           oauth_access_token_secret=oauth_access_token_secret)

    from pprint import pprint
    try:
        print "My info"
        pprint(client.auth.get_info())
        print "Team rooms:"
        pprint(client.hr.get_user_me())

    except Exception, e:
        print "Exception at %s %s" % (client.last_method, client.last_url)
    return client


def format_to_text(res, header):
    """
    :param res: res = [{u'c': [{u'v': u'anawaz1576'}, {u'v': u'Ali Nawaz'}, {u'v': u'140'}]}, ]}]
    :param header: '2016-09-15'
    :return: formated for slack str
    """
    import locale
    need_len = 45
    lc = lambda x: locale.currency(float(x), grouping=True)
    change_len = lambda line: line.replace('__', ' ' * (need_len - len(line.strip())))
    try:
        locale.setlocale(locale.LC_ALL, 'Usa')
        locale._override_localeconv = {'n_sign_posn': 1}
    except:
        locale.setlocale(locale.LC_ALL, b'en_US.UTF-8')

    charges_list = []
    summary = 0
    for line in res:
        line = line['c']
        charge = {
            'name': line[0]['v'],
            'charge': line[1]['v'] if len(line) == 2 else str(float(line[1]['v']) * -1),
            'extra': ' (Fixed price)' if len(line) > 2 else ''
        }
        summary += float(charge['charge'])
        charges_list.append(charge)

    # sort
    def by_name(s):
        return s['name']

    charges_list = sorted(charges_list, key=by_name)
    output = '```' \
             '%s\n' % header
    for charge in charges_list:
        line = "%s__%s" % (charge['name'], lc(charge['charge']))
        output += change_len(line) + charge['extra'] + '\n'
    output += change_len('\nSummary__%s' % lc(summary)) + '\n```'
    return output
