from django.core.management.base import BaseCommand
from myslackbot.settings import MWS_BOT_API_KEY
from slackclient import SlackClient
from slashes.accounting.upworktool import get_today_sum
from slashes.threepl_bot import threepl_to_slack
from constance import config


class Command(BaseCommand):
    def handle(self, *args, **options):
        from gendo import Gendo
        message = threepl_to_slack()
        sc = SlackClient(MWS_BOT_API_KEY)
        sc.api_call(
            "chat.postMessage", channel=config.threepl_channel, text=message,
            username='Fulfillment_Bot', icon_emoji=':robot_face:'
        )

