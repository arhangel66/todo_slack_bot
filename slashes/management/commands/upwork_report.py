from django.core.management.base import BaseCommand
from myslackbot.settings import BOT_API_KEY
from slashes.accounting.upworktool import get_today_sum
from constance import config


class Command(BaseCommand):
    def handle(self, *args, **options):
        from gendo import Gendo
        gendo = Gendo(BOT_API_KEY)
        today_sum = get_today_sum()
        gendo.speak(today_sum, config.accounting_channel)
        gendo.speak(today_sum, "@arhangel662")