from django.http import JsonResponse
# from slackclient import SlackClient
import xml.etree.ElementTree as xmlParser
import requests
from time import gmtime, strftime


def threepl_to_slack():
    url = 'https://secure-wms.com/webserviceexternal/contracts.asmx'
    headers = {
        'Content-Type': 'text/xml; charset=utf-8',
        'SOAPAction': '"http://www.JOI.com/schemas/ViaSub.WMS/ReportStockStatus"',
    }
    payload = '''<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <userLoginData xmlns="http://www.JOI.com/schemas/ViaSub.WMS/">
          <ThreePLID>773</ThreePLID>
          <Login>newage</Login>
          <Password>nutra</Password>
        </userLoginData>
      </soap:Body>
    </soap:Envelope>
    '''

    xml = requests.post(url, data=payload, headers=headers).text
    xml_parsed = xmlParser.fromstring(xml.replace(u'\u2019', u"'").replace(u'\u2018', u"'").encode('utf-8'))
    message = '```'
    message += 'Hashtag Stats %s : \n' % strftime("%Y-%m-%d", gmtime())
    message += '{:12} {:10s} {:35s} {:10s} \n'.format('SKU', 'Item Id', 'Description', 'Sum Of On Hand',)
    items = xmlParser.fromstring(xml_parsed[0][0].text)

    data = {}
    for item in items:
        if item[0].text not in data:
            data[item[0].text] = [item[9].text, item[10].text, float(item[17].text)]
        else:
            data[item[0].text][2] += float(item[17].text)

    for item in data:
        message += '{:12} {:10s} {:35s} {:10s} \n'.format(
            str(item),
            str(data[item][0]),
            str(data[item][1]),
            str(data[item][2]),
        )

    message += '```'
    return message




# def send_slack_message_to_emailmktg(body):
#
#     token = "xoxp-17638965109-20862439029-23554390549-8ba44caab0"
#     sc = SlackClient(token)
#
#     sc.api_call(
#         "chat.postMessage", channel="#emailmktg", text=body,
#         username='email_bot', icon_emoji=':robot_face:'
#     )
#
#
# def send_slack_message_to_amazon(body):
#
#     token = ""
#     sc = SlackClient(token)
#
#     sc.api_call(
#         "chat.postMessage", channel="#az-general", text=body,
#         username='Fulfillment_Bot', icon_emoji=':robot_face:'
#     )

