import json

import re

import requests

from background_task import background
from braces.views import CsrfExemptMixin
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import View
from slashes.asanatool import AsanaTool


def answer(url, data_dict):
    session = requests.Session()
    headers = {'Content-Type': 'application/json'}
    session.post(url, data=json.dumps(data_dict), headers=headers)


class Todo(CsrfExemptMixin, View):
    commands = ['get', 'add', 'done']

    def get_param(self, line, identifer='#', regular=''):
        line += ' '
        if not regular:
            regular = r"\%s([^\s]+)\s" % identifer

        val = re.search(regular, line, re.M | re.I)
        if val:
            return val.group(1)
        return ''

    def get_params(self, post):
        line = post.get('text')
        command = line.split(' ')[0]
        username = self.get_param(line, '@')
        channel = self.get_param(line, '#')
        if not channel and command != 'get':
            channel = post.get('channel_name')
        id = self.get_param(line, 'ID')
        remove_list = [command, ' @%s' % username, ' #%s' % channel, 'ID%s' % id]
        for item in remove_list:
            line = line.replace(item, '')
        line = line.strip()

        params = {
            'command': command,
            'username': username,
            'channel': channel,
            'id': id,
            'text': line,
            'url': post.get('response_url')
        }

        return params

    def get(self, request):
        return HttpResponse('get')

    def post(self, request):
        params = self.get_params(request.POST)
        if params.get('command') in self.commands:
            # start async command:
            command = getattr(self, "command_%s" % params.get('command'))
            command(params)
            return HttpResponse("%s" % request.POST.get('text'))
        else:
            return HttpResponse('error: command %s is unnown' % params.get('command'))

    @background
    def command_get(params):
        print 'command_get'
        asana = AsanaTool()
        result = asana.get_tasks(project_name=params.get('channel'), username=params.get('username'))
        print result
        answer(params.get('url'), {'text': result})

    @background
    def command_add(params):
        print 'command_add'
        asana = AsanaTool()
        result = asana.create_task_main(task_name=params.get('text'), username=params.get('username'), project_name=params.get('channel'))
        answer(params.get('url'), {'text': result})

    @background
    def command_done(params):
        print 'command_done'
        asana = AsanaTool()
        result = asana.done_task(id=params.get('id'))
        answer(params.get('url'), {'text': result})

        # print 'params = %s' % params
        # result = ''

        # else:
        #     result = 'error: corrent template is /todo TASKNAME @USERNAME'
        # session = requests.Session()
        # headers = {'Content-Type': 'application/json'}
        # session.post(request.POST.get('response_url'), data={'text': result}, headers=headers)
        # return HttpResponse("result: %s" % result)
