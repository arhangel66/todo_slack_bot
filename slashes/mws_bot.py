from django.http import JsonResponse
import xml.etree.ElementTree as xmlParser
import requests
import base64, hashlib, hmac, urllib
from time import gmtime, strftime


def index(request):
    return JsonResponse({'status': 'ok'})


def send_report(request):

    if 'message' in request.GET:
        token = "xoxp-17638965109-20862439029-23554390549-8ba44caab0"
        sc = SlackClient(token)

        sc.api_call(
            "chat.postMessage", channel="#alerts", text=request.GET['message'],
            username='server_bot', icon_emoji=':robot_face:'
        )

        return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': 'fail'})


def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(key, sig_data, hashlib.sha256)
    digest = hmac_obj.digest()

    return urllib.quote(base64.b64encode(digest), safe='-_+=/.~')


def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())

def get_today():
    """Return correctly QueryStartDateTime """
    return strftime("%Y-%m-%dT04:00:00Z", gmtime())


def mws_to_slack():
    SECRET_KEY = '6D8CXJYMfumbQzl0rfW3TKwlHYASxexLJKrewKp/'
    AWS_ACCESS_KEY = 'AKIAJMPW3NWOAKNLIZ7Q'
    SELLER_ID = 'AAT3GRXZY9CQN'

    Action = 'ListInventorySupply'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'
    URI = '/FulfillmentInventory/2010-10-01'
    domain = 'mws.amazonservices.com'
    proto = 'https://'
    method = 'POST'

    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'SellerId': SELLER_ID,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
        'SignatureMethod': SignatureMethod,
        'SellerSkus.member.1': 'GCE01-100060P2-FBA',
        'SellerSkus.member.2': 'SUPS-CHOC32P1-FBA',
        'SellerSkus.member.3': 'SUBB-BLU28P1-FBA',
        'SellerSkus.member.4': 'GCE01-100060P1-FBA',
        'SellerSkus.member.5': 'TTLCLS-60CP1-FBA'
    }

    request_description = '&'.join(
        ['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])

    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)

    url = '%s%s?%s&Signature=%s' % \
          (proto + domain, URI, request_description, urllib.quote(sig))

    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }

    xml = requests.request(method, url, headers=headers).text
    xml_parsed = xmlParser.fromstring(xml)
    xmlns = xml_parsed.tag.split('}')[0].replace('{', '')
    message = '```'
    message += 'Amazon Stats %s : \n' % strftime("%Y-%m-%d", gmtime())
    message += '{:25s} {:15s} {:5s} \n'.format('Product', 'ASIN', 'In Stock')
    for item in xml_parsed[0][1]:
        asin = item.find('{%s}ASIN' % xmlns).text if item.findall('{%s}ASIN' % xmlns) else ''
        instock = item.find('{%s}InStockSupplyQuantity' % xmlns).text if item.findall('{%s}InStockSupplyQuantity' % xmlns) else ''
        message += '{:25s} {:15s} {:5s} \n'.format(assin_map(asin), str(asin), str(instock))

    message += '```'
    return message


def assin_map(asin):
    map = {
        'B018T344W4': 'GARCINIA (Single)',
        'B01BMH7UIY': 'GARCINIA (Pack of 2)',
        'B01C7FQZV8': 'SLIMUP SHAKE',
        'B01C7FQZS6': 'SLIMUP SHAKE',
        'B01DTKNTLY': 'CLEANSE',
        'B01GSFKWJO': 'BLENDER BOTTLE',
        'B01J24P4SM': 'HORNY GOAT WEED (Single)',
        'B01J24P4QE': 'HORNY GOAT WEED (Pack of 2)',
        'B01J579X42': 'GARCINIA 98% HCA dropper (Single)',
        'B01J579X7E': 'GARCINIA 98% HCA dropper (Pack of 2)',
        'B01J5FNHN2': 'B12 Dropper (Single)',
        'B01J5FNHMI': 'B12 Dropper (Pack of 2)',
        'B01J5TGDAC': 'BellyTrim PILLS (Single)',
        'B01J5TGD7U': 'BellyTrim PILLS (Pack of 2)',
        'B01J6HUHQO': 'Forskolin PILLS (Single)',
        'B01J6HUHNM': 'Forskolin PILLS (Pack of 2)',
        'B01J687FAE': 'Green Coffee PILLS (Single)',
        'B01J687H9I': 'Forskolin PILLS (Pack of 2)',
    }
    return map.get(asin, 'Unnown')

