class AsanaTool(object):
    def __init__(self):
        import asana as asana
        from django.conf import settings
        self.client = asana.Client.access_token(settings.ASANA_KEY)
        self.errors = []

    def get_workspace_id(self):
        workspaces = self.client.workspaces.find_all({'opt_fields': 'id'})
        for ws in workspaces:
            return ws['id']
        self.errors.append('cant select workspaces')
        return None

    def get_or_create_project_id(self, project_name='', workspace_id=None):
        projects = self.client.projects.find_by_workspace(workspace_id)

        for project in projects:
            if project.get('name') == project_name:
                return project['id']

        project = self.client.projects.create_in_workspace(workspace_id, {'name': project_name})
        if not project:
            self.errors.append('cant create project')
        return project['id']

    def get_user_id(self, username, workspace_id):
        if username == 'me':
            return self.client.users.me()['id']
        users = self.client.users.find_by_workspace(workspace_id, {'opt_fields': 'name'})
        for user in users:
            # print user.get('name'), username
            if user.get('name') == username:
                return user.get('id')
        self.errors.append('cant find user by name %s' % username)
        return None

    def create_task(self, task_name, workspace_id, project_id, user_id):
        task = self.client.tasks.create_in_workspace(workspace_id,
                                                     {'name': task_name, 'projects': [project_id], 'assignee': user_id})
        return task

    def create_task_main(self, task_name, username, project_name):
        workspace_id = self.get_workspace_id()
        project_id = None
        user_id = None
        if workspace_id:
            project_id = self.get_or_create_project_id(project_name, workspace_id)
            user_id = self.get_user_id(username, workspace_id)

        if not self.errors:
            task = self.create_task(task_name, workspace_id, project_id, user_id)
            return "Task id:%s successfully created" % task['id']
        else:
            return "Errors: %s" % self.errors


    def get_tasks(self, project_name=None, username=None):
        workspace_id = self.get_workspace_id()
        print 60, project_name, project_name is None
        if not project_name:
            search_params = {'completed_since': '2117-04-18T12:12:12+0000', 'opt_fields': ['projects.name', 'name', 'completed']}
        else:
            search_params = {'completed_since': '2117-04-18T12:12:12+0000', 'opt_fields': ['assignee.name', 'name', 'completed']}

        if workspace_id:
            if project_name:
                project_id = self.get_or_create_project_id(project_name, workspace_id)
                search_params['project'] = project_id

            if username:
                user_id = self.get_user_id(username, workspace_id)
                search_params['assignee'] = user_id
                search_params['workspace'] = self.get_workspace_id()

        if not self.errors:
            tasks = self.client.tasks.find_all(search_params, page_size=99, )
            result = ''
            for task in tasks:
                assignee, projects = None, None
                if task.get('assignee'):
                    assignee = task['assignee'].get('name')
                if task.get('projects'):
                    projects = task['projects'][0].get('name')

                result_line = ''
                if project_name:
                    result_line = "%s, @%s, ID%s" % (task['name'], assignee, task['id'])
                if username:
                    result_line = "%s, #%s, ID%s" % (task['name'], projects, task['id'])
                result += result_line + '\n'
            return result
        else:
            return "Errors: %s" % self.errors

    def done_task(self, id):
        try:
            tasks = self.client.tasks.update(id, {'completed': True})
            return 'The task marked as completed'
        except:
            return 'Error, cant find the task'
