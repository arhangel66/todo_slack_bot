from pprint import pprint

import os

from django.core.wsgi import get_wsgi_application


os.environ['DJANGO_SETTINGS_MODULE'] = 'myslackbot.settings'
application = get_wsgi_application()

from django.core.mail import send_mail

from slashes.accounting.upworktool import get_today_sum, get_client, get_day_sum, get_day_sum_with_users, format_to_text
from slashes.threepl_bot import threepl_to_slack
from slashes.mws_bot import mws_to_slack

# print(threepl_to_slack())
# print(mws_to_slack())
today_sum = get_today_sum()
print(today_sum)
# res = get_today_sum()
# get_client()
# print(res)
# from django.conf import settings
# import datetime
# today = (datetime.datetime.now() - datetime.timedelta(hours=12)).strftime("%Y-%m-%d")
# print(today)
# from slashes.accounting.upworktool import get_today_sum, get_client, get_day_sum, get_day_sum_with_users, format_to_text
# client = get_client()
# company_id = '_kaizen'
# query = "SELECT amount WHERE date = '2016-09-29'"
# query2 = "SELECT sum(charges) WHERE worked_on = '%s'" % '2016-09-29'
# query3 = "SELECT provider_id, provider_name, date, amount WHERE date >= '2016-10-29' and subtype = 'Fixed Price'"
# company_id = '158758'
# client.timereport.get_company_report(company_id, query2, hours=False)
# client.finreport.get_provider_teams_billings(company_id, query)
# client.finreport.get_provider_earnings(company_id, query)
# client.finreport.get_buyer_teams_billings(company_id, query)
# client.finreport.get_financial_entities('570680', query3)
#
# from slashes.accounting.upworktool import *
# client = get_client()
# today = '2016-09-29'
# company_id = u'_kaizen'
# today_res = get_day_sum_with_users(today, client, company_id)
# message = format_to_text(today_res, today)
# print(message)
#
# query3 = "SELECT provider_id, provider_name, date, amount WHERE date >= '2016-10-01' and subtype = 'Fixed Price'"
# client.finreport.get_financial_entities_provider('570680', query3)  # it is freelancer
# query3 = "SELECT provider_name, amount, type, description, subtype  WHERE date >= '2016-08-29' and type='ARInvoice'"
# pprint(client.finreport.get_financial_entities_provider('570680', query3))
#
#
# query3 = "SELECT provider_name, amount, subtype WHERE date = '2016-10-01' and subtype = 'Fixed Price' and type='ARInvoice'"
# result = client.finreport.get_financial_entities_provider('570680', query3)
# res = result.get('table', {}).get('rows')
# print(res)

# client.finreport.get_buyer_companies_billings('158758', query)

# client.finreport.get_financial_entities('570680', query)
#
# client = get_client()
# # week_worked_on - '20160912'
# # assignment_team_id - _kaizen
# # provider_id - arhangel662
# #
# query = "SELECT provider_id, provider_name, sum(charges) WHERE worked_on = '2016-09-15'"
# result = client.timereport.get_company_report('_kaizen', query)
# res = result.get('table', {}).get('rows')
#
# get_day_sum
#
#
#
# res = [{u'c': [{u'v': u'anawaz1576'}, {u'v': u'Ali Nawaz'}, {u'v': u'140'}]}, {u'c': [{u'v': u'arhangel662'}, {u'v': u'Mikhail Derbichev'}, {u'v': u'41.666667'}]}, {u'c': [{u'v': u'goblin72'}, {u'v': u'Mikhail Taranyuk'}, {u'v': u'212.98'}]},
# {u'c': [{u'v': u'arunragini'}, {u'v': u'Deepa Ragini'}, {u'v': u'55.566667'}]}, {u'c': [{u'v': u'kentrobert'}, {u'v': u'Robert Kent'}, {u'v': u'11'}]}]
#
# # output = format_to_text(res, 'test')
# # print(output)
#
# team_lists = client.hr.get_teams()
# for team_obj in team_lists:
#     print team_obj['id'], team_obj['name'],
#     try:
#         print(get_day_sum('2016-09-29', client, '_kaizen'))
#     except:
#         print('error')
#
# from slashes.accounting.upworktool import get_today_sum, get_client, get_day_sum, get_day_sum_with_users, format_to_text
# today_res = get_day_sum_with_users('2016-09-29', client, '_kaizen')
# print(format_to_text(today_res))



# print result.get('table')
# test = {u'rows': [{u'c': [{u'v': u'20160912'}, {u'v': u'_kaizen'}, {u'v': u'arunragini'}, {u'v': u'Linux, nginx, php server admin'}]}, {u'c': [{u'v': u'20160912'}, {u'v': u'_kaizen'}, {u'v': u'arhangel662'}, {u'v': u'Django developer for our  team'}]}, {u'c': [{u'v': u'20160912'}, {u'v': u'_kaizen'}, {u'v': u'goblin72'}, {u'v': u'Java developer: REST, JSON,  SERVLETS, DB Knowledge required'}]}, {u'c': [{u'v': u'20160912'}, {u'v': u'_kaizen'}, {u'v': u'anawaz1576'}, {u'v': u'Full time Java developer'}]}, {u'c': [{u'v': u'20160912'}, {u'v': u'_kaizen'}, {u'v': u'kentrobert'}, {u'v': u'Amazon Selling Virtual Assistant'}]}], u'cols': [{u'type': u'date', u'label': u'week_worked_on'}, {u'type': u'string', u'label': u'assignment_team_id'}, {u'type': u'string', u'label': u'provider_id'}, {u'type': u'string', u'label': u'assignment_name'}]}
# pprint(test)
# url = "https://www.upwork.com/ab/feed/jobs/rss?client_hires=1-9%2C10-&q=skills%3A%28django-framework%29+%7C%7C+django&sort=create_time+desc&job_type=hourly&api_params=1&securityToken=a771fa05c25778cbd0112a51311eb4e926df2adfbef7f02bf2d1de21f17478af6127bc0b1a01bbce25550f09b4f2d0f4e06d13b2a340b0e6880d1fadeb286b81"
from lxml import etree
# import requests
# resp = requests.get(url)
# print resp.content
# tree = etree.parse(url)
# import xml.etree.ElementTree
# e = xml.etree.ElementTree.parse(url).getroot()
# print e
# class Test(object):
#     pass

# print item
# tree = etree.fromstring(resp.content, base_url=url)
# print tree

# from chump import Application
# app = Application('ayug72sbn9zitysw98pxary7kygzx4')
# print app.is_authenticated
#
# user = app.get_user('uab5t1t2r5q6oc8824zph8ahunmibo')
# print user.is_authenticated, user.devices
# message = user.send_message("What's up, dog?")
# message.is_sent, message.id, str(message.sent_at)

# send_mail(
#     "Enquiry [from NGM website]",
#     'test',
#     "nicholas@gifford-mead.co.uk",
#     ['arhangel662@gmail.com'],
#     fail_silently=False
# )
# email = EmailMessage('Hi!', 'Cool message for %recipient.first_name%', 'admin@example.com', [joe@example.com, jane@example.com])
# email.extra_headers['recipient_variables'] = '{"joe@example.com":{"first_name":"Joe"}, "jane@example.com":{"first_name":"Jane"}}'
# email.send()

from django.core.mail import EmailMessage

# email = EmailMessage('title', 'body', to=['arhangel662@gmail.com'])
# email.send()
from django.test.client import RequestFactory, Client

# os.environ['HTTPLIB_CA_CERTS_PATH'] = '/path/to/my/ca_certs.txt'
# from django.conf import settings
# import upwork
# client = upwork.Client(settings.UPWORK_KEY.get('key'), settings.UPWORK_KEY.get('secret'))
# print client.auth.get_authorize_url()

# /todo TASKNAME @USERNAME  project name = room name

# def send_test_post(command='asdf @irina.tsarkova'):
#     test_dict = {u'channel_name': 'testchannel', u'command': u'/todo', u'team_id': u'T0YLG1R7S',
#                  u'text': command, u'token': u'nB63xmviOOrI69G88nj5A68w', u'team_domain': u'sky-hq',
#                  u'channel_id': u'C0YLNTHFH', u'user_id': u'U0YK233GB',
#                  u'response_url': u'https://hooks.slack.com/commands/T0YLG1R7S/35553763664/4Dxfxu7WFFROKlp298ZEyBH2',
#                  u'user_name': u'arhangel662'}
#     c = Client()
#     response = c.post('/commands/todo/', test_dict)
#     print response.content

# add task to project of channelname:
# /todo add  TASKNAME @rovin
#
# list projects:
# /todo get #channel
# /todo get @rovin
#
# mark done:
# /todo done [asana ID, number, task]


commands = [
    # 'add taskname @irina.tsarkova',
    # 'get #testchannel',
    # 'get @me',
    # 'get2',
    # 'get #channel @rovin',
    # 'done ID109723647156079',
    # 'get @irina.tsarkova',

]
import re
# for command in commands:
#     command += ' '
#     channel = re.search(r"\#([^\s]+)\s", command, re.M | re.I)
#     if channel:
#         print 'chan=' + channel.group(1)
#
#     user = re.search(r"\@([^\s]+)\s", command, re.M | re.I)
#     if user:
#         print 'user=' + user.group(1)


# from slashes.upworktool import *
#
# client = get_client_my()




# tags_dict = tags.split(',')
# given_tags = video['tags'].split(',')

# for command in commands:
#     print command
#     send_test_post(command=command)
#     print ''
# import requests
# import json
# session = requests.Session()
# headers = {'Content-Type': 'application/json'}
# response = session.post('https://hooks.slack.com/commands/T0YLG1R7S/34633167108/SFrTrqJ81a6QKh72R2hUmwwC', data=json.dumps({'text': 'command_get'}), headers=headers)
# print 12, response.content, 33



# asana = AsanaTool()
# print asana.create_task_main(task_name='new task2', username='me', project_name='new project2')


# workspace_id =
# for project in client.projects.find_all({'workspace': workspace_id}):
#     print project, project['id']
# project = client.projects.create_in_workspace(workspace_id, { 'name': 'new project' })

# def create_task_in_project(task_name = 'task_name', project_name)
# task = client.tasks.create_in_workspace(workspace_id, {'name': 'task_name'})
# print "Created project with id: %s" % task['id']

# client.tasks.add_project(task['id'], {'project': project['id']})
# for task in client.tasks.find_all():
#     print task
