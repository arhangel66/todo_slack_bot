from django.conf.urls import include, url
from django.contrib import admin
from slashes.views import Todo

urlpatterns = [
    # Examples:
    url(r'^commands/todo/$', Todo.as_view(), name='todo'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
]
