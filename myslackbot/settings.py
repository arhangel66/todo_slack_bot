"""
Django settings for slackbot project on Heroku. Fore more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "4&nedh)@a-_^80@sn!lqg&i6pey4bvk-^xc7c+pbts!o2@=%o&"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'slashes',
    'background_task',
    # 'anymail',
    'constance',
    'constance.backends.database',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'myslackbot.urls'

TEMPLATES = (
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
)

WSGI_APPLICATION = 'myslackbot.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = (
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
)

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Update database configuration with $DATABASE_URL.
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
# ASANA_KEY = '0/14acfd6e6369a0f63af45b9e9771db50'  # me
ASANA_KEY = '0/6876288a6f5c1eeebfe487581a743051'

# Settings for heroku slack bot
U_KEY = '1391b7b7e10b388063ff548544e44520'
U_SECRET = 'cb035ed562bf17bb'
U_OAUTH_ACCESS_TOKEN = '2c89818270108a30ecf3c09d59f336c5'
U_OAUTH_ACCESS_TOKEN_SECRET = 'c3831e24d8f1a5c4'

ACCEPTED_USERS = ('arhangel662', 'rovin')
ACCEPTED_CHANNELS = ('accounting')

BOT_API_KEY = "xoxb-42517446417-6Z2Y27XRVeeZ82QJxO8FB0Xw"
MWS_BOT_API_KEY = "xoxp-17638965109-20862439029-23554390549-8ba44caab0"
# ENQUIRY_EMAIL = "nicholas@gifford-mead.co.uk"

# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_USE_TLS = True
# EMAIL_HOST = "smtp.gmail.com"
# # EMAIL_HOST_USER = "nicholasgiffordmead@gmail.com"
# # EMAIL_HOST_PASSWORD = "pimlico68new"
# EMAIL_HOST_USER = "68ngm68@gmail.com"
# EMAIL_HOST_PASSWORD = "henryjames"
# EMAIL_PORT = 587
# EMAIL_SUBJECT_PREFIX = "[nicholasgiffordmead.co.uk]"

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = "nicholas@gifford-mead.co.uk"
EMAIL_HOST_PASSWORD = "68hampson68"
EMAIL_PORT = 587
 # and the email address is

ANYMAIL = {
    "MAILGUN_API_KEY": "key-3a1f9aeee96e992d1e4f50841e00943c",
}
EMAIL_BACKEND = "anymail.backends.mailgun.MailgunBackend"  # or sendgrid.SendGridBackend, or...
DEFAULT_FROM_EMAIL = "main@giffordmead.webfactional.com"  # if you don't already have this in settings


CONSTANCE_ADDITIONAL_FIELDS = {
    'yes_no_null_select': ['django.forms.fields.ChoiceField', {
        'widget': 'django.forms.Select',
        'choices': (("-----", None), (True, "Yes"), (False, "No"))
    }],
    'yes_no_select': ['django.forms.fields.ChoiceField', {
        'widget': 'django.forms.Select',
        'choices': ((True, "Yes"), (False, "No"))
    }],
}

CONSTANCE_CONFIG = {
    'mws_channel': ('#team_ecommerce', 'Channel for posting mws reports'),
    'threepl_channel': ('#team_ecommerce', 'Channel for posting threepl reports'),
    'accounting_channel': ('#_accounting', 'Channel for posting accounting reports'),
}
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'